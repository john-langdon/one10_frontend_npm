export class ObjectType {
	private static instance: ObjectType;
	private static instance_index: number = -1;

	private constructor() {}

	static I(): ObjectType {
		if (!ObjectType.instance) {
			ObjectType.instance = new ObjectType();
			this.instance_index++;
			console.log(`ObjectType Instance Created. Num should Never be > 0: ${this.instance_index}`);
		}
		return ObjectType.instance;
	}

	public for = ($object) => {
		return Object.prototype.toString.call($object).slice(8, -1);
	};

	public is = ($object, $type): any => {
		return this.for($object) !== undefined && $object !== null && this.for($object) === true;
	};

	public log = ($msg: any) => console.log($msg);
}

/*
    Use:
*/
/*

////
///
//
//    Example
//
///
////
class SomeUnknownClass {
	constructor() {
		console.log('Some Unknown Class constructor called');
	}
}

const ObjectFAQ = ObjectType.I();
ObjectFAQ.log('Message logged using singleton method');

let unknown_class = new SomeUnknownClass();
var typeOf__unknown_class = ObjectFAQ.for(unknown_class);
console.log(typeOf__unknown_class);
*/